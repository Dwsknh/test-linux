- Install linux (any distro, debian-based is preferred)

- Use LXQt as desktop environment

- Configure the DE, and remove sysbar panel at the bottom of the screen

- Create a service stated in this [link](https://gitlab.com/mbkm/enrollment-test/linux-devops/-/blob/master/02-Service.MD)

- Create a script to list HID and Storage Devices connected through USB

- Create a script to update, upgrade, and remove unused packages

- Create an autorun script to show a terminal window when DE is loaded
